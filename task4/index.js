const ERROR_MESSAGES = {
  'NO_STOCK': 'No stock has been found',
  'INCORRECT_DETAILS': 'Incorrect details have been entered',
};

const getResponseByState = ({ state: stateName, errorCode }) => {
  if (stateName === 'error') {
    return {
      title: 'Error page',
      message: ERROR_MESSAGES[errorCode] || null,
    }
  }

  return {
    title: 'Order complete',
    message: null,
  }
};

const getProcessingPage = async (states) => {
  const [firstState, nextState] = states;

  return new Promise((resolve) => {
    if (firstState.state === 'processing') {
      setTimeout(() => {
        resolve(getResponseByState(nextState));
      }, 2000);
      return;
    }
    resolve(getResponseByState(firstState));
  });
};


getProcessingPage([
  { state: 'processing' },
  { state: 'error', errorCode: 'NO_STOCK' },
]).then(result => console.log(result));

