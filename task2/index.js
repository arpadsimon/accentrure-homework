import Provider from './provider.js';

async function getCityInfoByCoordinates(long, lat) {
  try {
    const city = await Provider.findCity(long, lat);
    const weather = await Provider.getWeather(city);
    const currency = await Provider.getLocalCurrency(city);

    console.log(city);
    console.log(weather);
    console.log(`${weather}. ${currency}.`);
  } catch (e) {
    console.log('Oops, something went wrong...');
  }
}

getCityInfoByCoordinates(0.1278, 51.507);
